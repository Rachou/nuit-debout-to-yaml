#! /usr/bin/env python3


# nuit-debout-to-yaml -- Convert Nuit Debout catalog of services to a Git repository of YAML files
# By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
#
# Copyright (C) 2016 Etalab
# https::#git.framasoft.org/etalab/nuit-debout-to-yaml
#
# nuit-debout-to-yaml is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# nuit-debout-to-yaml is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


import argparse
import collections
import os
import sys
import urllib.request

import lxml.html
from slugify import slugify
import yaml
try:
    from yaml import CDumper as Dumper
except ImportError:
    from yaml import Dumper


# YAML configuration


class folded_str(str):
    pass


class literal_str(str):
    pass


def dict_representer(dumper, data):
    return dumper.represent_dict(sorted(data.iteritems()))


yaml.add_representer(folded_str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str',
    data, style='>'))
yaml.add_representer(literal_str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str',
    data, style='|'))
yaml.add_representer(collections.OrderedDict, dict_representer)
yaml.add_representer(str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str', data))


#


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('yaml_dir', help = 'path of target directory for generated YAML files')
    args = parser.parse_args()

    if not os.path.exists(args.yaml_dir):
        os.makedirs(args.yaml_dir)

    url = "https://wiki.nuitdebout.fr/wiki/Ressources/Liste_d%27outils_num%C3%A9riques"
    response = urllib.request.urlopen(url)
    html = response.read().decode("utf-8")
    html_element = lxml.html.document_fromstring(html)
    table_elements = html_element.xpath('//table')
    assert len(table_elements) == 1
    table_element = table_elements[0]

    tr_elements = table_element.xpath('./tr')
    labels = [
        th_element.text.strip()
        for th_element in tr_elements[0].xpath('./th')
        ]
    assert labels == [
        'Outil',
        'Fonction',
        'Libre',
        'URL',
        'Détails',
        'Aide à la prise en main',
        'Lien vers le code',
        'Nom de la licence',
        'Nb de développeurs',
        ], labels

    for tr_element in tr_elements[1:]:
        entry = collections.OrderedDict()
        for label, td_element in zip(labels, tr_element.xpath('./td')):
            if label == 'Aide à la prise en main':
                value = lxml.html.tostring(td_element, encoding = 'unicode', with_tail = False).strip().lstrip('<td>') \
                    .rstrip('</td>').strip()
                if value:
                    entry[label] = value
            elif label == 'Détails':
                value = lxml.html.tostring(td_element, encoding = 'unicode', with_tail = False).strip().lstrip('<td>') \
                    .rstrip('</td>').strip()
                if value:
                    entry[label] = value
            elif label == 'Fonction':
                value = lxml.html.tostring(td_element, encoding = 'unicode', method = 'text').strip()
                if value:
                    entry[label] = value
            elif label == 'Libre':
                value = lxml.html.tostring(td_element, encoding = 'unicode', method = 'text').strip().lower()
                value = {
                    '': None,
                    '?': None,
                    'non': False,
                    'non (serveur)oui (client)': {'client': True, 'serveur': False},
                    'oui': True,
                    'oui\xa0?': None,
                    'serveur': {'client': False, 'serveur': True},
                    }.get(value, value)
                assert not isinstance(value, str), value
                if value is not None:
                    entry[label] = value
            elif label == 'Lien vers le code':
                urls = []
                for td_child in td_element:
                    if td_child.tag == 'a':
                        url = td_child.attrib['href'].strip()
                        urls.append(url)
                    elif td_child.tag == 'p':
                        for p_child in td_child:
                            if p_child.tag == 'a':
                                url = p_child.attrib['href'].strip()
                                urls.append(url)
                            else:
                                assert False, lxml.html.tostring(p_child, encoding = 'unicode')
                    else:
                        assert False, lxml.html.tostring(td_child, encoding = 'unicode')
                if urls:
                    assert len(urls) == 1, urls
                    entry[label] = urls[0]
            elif label == 'Nb de développeurs':
                assert len(td_element) == 0, lxml.html.tostring(td_element, encoding = 'unicode')
                value = td_element.text.strip()
                if value:
                    entry[label] = value
            elif label == 'Nom de la licence':
                value = lxml.html.tostring(td_element, encoding = 'unicode', method = 'text').strip()
                if value:
                    entry[label] = value
            elif label == 'Outil':
                assert len(td_element) == 0, lxml.html.tostring(td_element, encoding = 'unicode')
                value = td_element.text.strip()
                if value:
                    entry[label] = value
            elif label == 'URL':
                urls = []
                for a_child in td_element.xpath('.//a'):
                    url = a_child.attrib['href'].strip()
                    urls.append(url)
                if urls:
                    entry[label] = urls
            else:
                raise AssertionError("Unknown label: {}".format(label))
        slug = slugify(entry['Outil'])
        with open(os.path.join(args.yaml_dir, '{}.yaml'.format(slug)), 'w') as yaml_file:
            yaml.dump(entry, yaml_file, allow_unicode = True, default_flow_style = False, Dumper = Dumper,
                indent = 2, width = 120)

    return 0


if __name__ == '__main__':
    sys.exit(main())
